﻿using Movies.Models;
using X.PagedList;

namespace Movies.BLL.Infrastructure
{
    public interface IService<T> where T : class
    {
        IPagedList<T> GetAll(int page);
        MovieModel Get(int id);
        void Create(MovieModel model);
        void Update(MovieModel model);
        void Delete(int id);
    }
}
