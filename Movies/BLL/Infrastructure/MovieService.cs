﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Movies.DAL.Entities;
using Movies.DAL.Interfaces;
using Movies.Models;
using X.PagedList;

namespace Movies.BLL.Infrastructure
{
    public class MovieService : IService<MovieModel>
    {
        private readonly IRepository<Movie> _repository;
        private const int pageSize = 5;

        public MovieService(IRepository<Movie> repository)
        {
            _repository = repository;
        }
        public IPagedList<MovieModel> GetAll(int page)
        {
            return Mapper.Map<IEnumerable<MovieModel>>(_repository.GetAll()).ToPagedList(page, pageSize);
        }

        public MovieModel Get(int id)
        {
            return Mapper.Map<MovieModel>(_repository.Get(id));
        }

        public void Create(MovieModel model)
        {
            string fileName = Path.GetFileName(model.ImageFile.FileName);
            model.ImageFile.SaveAs(HttpContext.Current.Server.MapPath("/Files/" + fileName));
            string currUserId = HttpContext.Current.User.Identity.GetUserId();
            model.UserId = currUserId;
            model.ImagePath = "/Files/" + fileName;
            _repository.Create(Mapper.Map<Movie>(model));
        }

        public void Update(MovieModel model)
        {
            string fileName = Path.GetFileName(model.ImageFile.FileName);
            model.ImageFile.SaveAs(HttpContext.Current.Server.MapPath("/Files/" + fileName));
            string currUserId = HttpContext.Current.User.Identity.GetUserId();
            model.UserId = currUserId;
            model.ImagePath = "/Files/" + fileName;
            _repository.Update(Mapper.Map<Movie>(model));
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }
    }
}