﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using AutoMapper.EquivalencyExpression;

namespace Movies.Configuration
{
    public static class AutomapperInit
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new DbMapperConfiguration());
                cfg.AddCollectionMappers();
            });
            Mapper.AssertConfigurationIsValid();
        }
    }
}