﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Movies.DAL.Entities;
using Movies.Models;

namespace Movies.Configuration
{
    public class DbMapperConfiguration : Profile
    {
        public DbMapperConfiguration()
        {
            CreateMap<Movie, MovieModel>()
                .ForMember(x => x.ImageFile, s => s.Ignore());
            CreateMap<MovieModel, Movie>()
                .ForMember(x => x.User, s => s.Ignore());
        }
    }
}