﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Movies.Models
{
    public class MovieModel
    {
        public int Id { get; set; }

        [Display(Name = "Название фильма")]
        [Required]
        public string Name { get; set; }

        public string ImagePath { get; set; }

        [Display(Name = "Постер")]
        [Required]
        [RegularExpression(@"([a-zA-Zа-яА-Я0-9\s_\\.\-:])+(.png|.jpg|.gif)$", ErrorMessage = "Only Image files allowed.")]
        public HttpPostedFileBase ImageFile { get; set; }

        [Display(Name = "Описание фильма")]
        [Required, MinLength(150, ErrorMessage = "Описание фильма должно быть не менее 150 символов")]
        public string Description { get; set; }

        [Display(Name = "Год выпуска")]
        [Required]
        public int ProductionYear { get; set; }

        [Display(Name = "Режиссер")]
        [Required, MinLength(5, ErrorMessage = "Имя должно быть более 5 символов"), ]
        [RegularExpression(@"([a-zA-Zа-яА-Я])$", ErrorMessage = "Only letters allowed.")]
        public string DirectorName { get; set; }
        public string UserId { get; set; }
    }
}