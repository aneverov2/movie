﻿using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Movies.BLL.Infrastructure;
using Movies.Models;

namespace Movies.Controllers
{
    public class MovieController : Controller
    {
        readonly IService<MovieModel> _service;

        public MovieController(IService<MovieModel> service)
        {
            _service = service;
        }

        // GET: Movie
        public ActionResult Index(int page = 1)
        {
            return View(_service.GetAll(page));
        }

        // GET: Movie/Details/5
        public ActionResult Details(int id)
        {
            return View(_service.Get(id));
        }

        // GET: Movie/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Movie/Create
        [HttpPost]
        public ActionResult Create(MovieModel movie)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _service.Create(movie);

                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    throw new HttpException(e.Message);
                }
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Movie/Edit/5
        public ActionResult Edit(int id)
        {
            MovieModel movie = _service.Get(id);
            if (System.Web.HttpContext.Current.User.Identity.GetUserId() == movie.UserId)
            {
                return View(_service.Get(id));
            }

            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        // POST: Movie/Edit/5
        [HttpPost]
        public ActionResult Edit(MovieModel movie)
        {
            try
            {
                _service.Update(movie);
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                throw new HttpException(e.Message);
            }
        }

        // GET: Movie/Delete/5
        public ActionResult Delete(int id)
        {
            return View(_service.Get(id));
        }

        // POST: Movie/Delete/5
        [HttpPost]
        public ActionResult DeleteItem(int id)
        {
            try
            {
                _service.Delete(id);

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                throw new HttpException(e.Message);
            }
        }
    }
}
