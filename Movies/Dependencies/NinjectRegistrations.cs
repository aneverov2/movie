﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Movies.BLL.Infrastructure;
using Movies.DAL.Entities;
using Movies.DAL.Interfaces;
using Movies.DAL.Repositories;
using Movies.Models;
using Ninject.Modules;

namespace Movies.Dependencies
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<IRepository<Movie>>().To<MovieRepository>();
            Bind<IService<MovieModel>>().To<MovieService>();
        }
    }
}