﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Movies.Models;

namespace Movies.DAL.Entities
{
    public class Movie
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }
        public int ProductionYear { get; set; }
        public string DirectorName { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}