﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Microsoft.AspNet.Identity;
using Movies.Data;
using Movies.DAL.Entities;
using Movies.DAL.Interfaces;
using Movies.Models;

namespace Movies.DAL.Repositories
{
    public class MovieRepository : IRepository<Movie>
    {
        private readonly ApplicationDbContext _context;

        public MovieRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IQueryable<Movie> GetAll()
        {
            return _context.Movies;
        }

        public Movie Get(int id)
        {
            return _context.Movies.Find(id);
        }

        public Movie SingleOrDefault(Func<Movie, bool> predicate)
        {
            return _context.Movies.SingleOrDefault(predicate);
        }

        public IQueryable<Movie> Find(Expression<Func<Movie, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public void Create(Movie item)
        {
            _context.Movies.Add(item);
            _context.SaveChanges();
        }

        public void Update(Movie item)
        {
            _context.Entry(item).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            Movie movie = _context.Movies.Find(id);
            if (movie != null)
                _context.Movies.Remove(movie);
            _context.SaveChanges();
        }
    }
}